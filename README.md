# IPBX
test
>>>>>>> 669e4401c03f17cb26d5fc20d7945fa43bf10ba3
# VISION PRODUIT
## Pour : 
        Une entreprise de vingt utilisateurs avec cinq services différents.
    
## Qui veulent : 
        - Le service RH composé de 3 utilisateurs.
        - Le service Direction composé de 3 utilisateurs.
        - Le service Informatique composé de 3 utilisateurs.
        - Le service Comptabilité composé de 3 utilisateurs.
        - Le service Employés composé de 8 utilisateurs.

## Produit : 
        - 1 serveur Asterisk sous l'OS Debian.
        - 20 softphones Blink.

## Est un : 
        - Asterisk : Serveur de téléphonie IP.
        - Blink : Émulateur de téléphone IP.

## Qui : 
        - Permet une communication téléphonique entre les utilisateurs dans un réseau privé.
        - Enregistre des messages sur des boîtes vocales personnelles au cas où un utilisateur répond pas.
        - Au cas où un utilisateur ne connait pas le numéro d'un de ces collègues, un IVR sera mit en
        place avec un choix pour chaques services et dans chacuns des services un choix pour chacuns
        des utilisateurs qui les composent.

## À la différence de :
        Utiliser un serveur héberger d'une entreprise préstataire.

## Notre produit : 
        Asterisk est un serveur complètement gratuit.

# GESTIONS DES RISQUES
## Technique :
        Serveur Asterisk qui tombe.
        Fréquence : faible
        Impact : très fort

        Problème de réseau.
        Fréquence : faible
        Impact : très fort
        
## Externes (clients, utilisateurs…) :
        Clients et utilisateurs exigeants.
        Fréquence : moyenne
        Impact : faible

## Organisationnel :
        Avoir un imprévu.
        Fréquence : faible
        Impact : dépend du problème

## Gestion de projet :
        Mauvaise gestion de son temps. 
        Fréquence : faible
        Impact : moyen
        
        Avoir des points bloquants.
        Fréquence : moyenne
        Impact : dépend du problème

        Avoir un cas particulier.
        Fréquence : moyenne
        Impact : faible

        Dépendre de quelqu'un.
        Fréquence : moyenne
        Impact : moyen

